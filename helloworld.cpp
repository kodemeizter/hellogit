#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main()
{	
	vector <int> num = {10, 20, 30, 40, 50, 60};
	num.push_back(70);
	for (int i=0; i<num.size(); i++){
		cout<<num[i]<<" ";
	}
	cout<<"\n";
	num.pop_back();
	for (int i=0; i<num.size(); i++){
		cout<<num[i]<<" ";
	}
	cout<<endl;
	string test1 = "Hello world, ";
	string intro = test1 + "my name is Kody";
	cout<<test1<<endl;
	cout<<intro<<endl;	
	cout<<"This string has "<<intro.size()<<" characters"<<endl;
	for( char ch: test1)
		cout<<ch<<endl;
	auto v= 10+51;
	cout<<v<<endl;
}

